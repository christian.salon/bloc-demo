import 'package:bloc_demo/cart/presentation/screen/cart_screen.dart';
import 'package:bloc_demo/post/presentation/screen/post_screen.dart';
import 'package:flutter/material.dart';

import 'login/presentation/screen/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),

      /// Example: BlocProvider and BlocBuilder, BlocSelector, BLocListener
      home: const PostScreen(),
      // home: const CartScreen(),

      /// Example: BlocListener
      // home: const LoginScreen(),
    );
  }
}
