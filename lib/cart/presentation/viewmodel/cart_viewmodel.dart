import 'package:flutter_bloc/flutter_bloc.dart';

import '../event/cart_event.dart';
import '../state/cart_state.dart';

class CartViewModel extends Bloc<CartEvent, CartState> {
  CartViewModel() : super(const CartState()) {
    on<CartAdded>(_onCartAdded);
  }

  void _onCartAdded(CartAdded event, Emitter<CartState> emit) {
    emit(state.copyWith(
      products: [...state.products, event.product],
    ));
  }
}