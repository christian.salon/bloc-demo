import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../screen/checkout_screen.dart';
import '../state/cart_state.dart';
import '../viewmodel/cart_viewmodel.dart';

class CartBadge extends StatelessWidget {
  const CartBadge({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartViewModel, CartState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () {
            if (state.products.isEmpty) {
              return;
            }

            /// Navigate to checkout screen
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => CheckOutScreen(products: state.products),
              ),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Badge.count(
              count: state.products.length,
              child: const Icon(Icons.shopping_cart),
            ),
          ),
        );
      }
    );
  }
}
