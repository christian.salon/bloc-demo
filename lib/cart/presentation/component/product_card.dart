import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget {
  final String name;
  final double price;
  final void Function() onTapped;

  const ProductCard({
    Key? key,
    required this.name,
    required this.price,
    required this.onTapped,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapped,
      child: Card(
        /// generate random background colors
        color: Colors.primaries[price.toInt() % Colors.primaries.length],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(name, style: const TextStyle(fontSize: 18)),
            Text('Price: $price', style: const TextStyle(fontSize: 12)),
          ],
        ),
      ),
    );
  }
}

