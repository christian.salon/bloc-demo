import '../../data/model/product.dart';

abstract class CartEvent {}

class CartAdded extends CartEvent {
  final Product product;

  CartAdded(this.product);
}