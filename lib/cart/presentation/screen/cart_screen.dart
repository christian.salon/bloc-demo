import 'package:bloc_demo/cart/presentation/event/cart_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/model/product.dart';
import '../component/cart_badge.dart';
import '../component/product_card.dart';
import '../viewmodel/cart_viewmodel.dart';

/// List of products
const products = [
  Product(
    name: 'Product 1',
    price: 100.0,
  ),
  Product(
    name: 'Product 2',
    price: 200.0,
  ),
  Product(
    name: 'Product 3',
    price: 300.0,
  ),
  Product(
    name: 'Product 4',
    price: 400.0,
  ),
  Product(
    name: 'Product 5',
    price: 500.0,
  ),
  Product(
    name: 'Product 6',
    price: 600.0,
  ),
  Product(
    name: 'Product 7',
    price: 700.0,
  ),
  Product(
    name: 'Product 8',
    price: 800.0,
  ),
  Product(
    name: 'Product 9',
    price: 900.0,
  ),
  Product(
    name: 'Product 10',
    price: 1000.0,
  ),
];

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CartViewModel>(
      create: (context) => CartViewModel(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Products'),
          actions: const [
            CartBadge(),
          ],
        ),
        body: GridView.builder(
          itemCount: products.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
          ),
          itemBuilder: (context, index) {
            final product = products[index];
            return ProductCard(
              name: product.name,
              price: product.price,
              onTapped: () {
                /// Add product to cart
                context.read<CartViewModel>().add(CartAdded(product));
              },
            );
          },
        )
      ),
    );
  }
}
