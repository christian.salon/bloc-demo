import 'package:flutter/material.dart';

import '../../data/model/product.dart';

class CheckOutScreen extends StatelessWidget {
  final List<Product> products;

  const CheckOutScreen({
    Key? key,
    required this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
        itemCount: products.length,
        padding: const EdgeInsets.all(8.0),
        itemBuilder: (context, index) {
          final product = products[index];
          return ListTile(
            tileColor: Colors.grey[200],
            title: Text(product.name),
            subtitle: Text(product.price.toString()),
          );
        },
      ),
    );
  }
}
