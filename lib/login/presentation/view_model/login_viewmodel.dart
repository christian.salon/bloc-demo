import 'package:bloc_demo/login/presentation/event/login_event.dart';
import 'package:bloc_demo/login/presentation/state/login_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginViewModel extends Bloc<LoginEvent, LoginState> {
  LoginViewModel() : super(const LoginState()) {
    on<LoginUsernameChanged>(_onUsernameChanged);
    on<LoginPasswordChanged>(_onPasswordChanged);
    on<LoginSubmitted>(_onSubmitted);
  }

  void _onUsernameChanged(
    LoginUsernameChanged event,
    Emitter<LoginState> emit,
  ) {
    emit(state.copyWith(username: event.username));
  }

  void _onPasswordChanged(
    LoginPasswordChanged event,
    Emitter<LoginState> emit,
  ) {
    emit(state.copyWith(password: event.password));
  }

  Future<void> _onSubmitted(
    LoginSubmitted event,
    Emitter<LoginState> emit,
  ) async {
    if (state.username.isEmpty || state.password.isEmpty) {
      emit(state.copyWith(status: LoginStatus.error));
      return;
    }

    emit(state.copyWith(status: LoginStatus.loading));
    await Future.delayed(const Duration(seconds: 2));
    emit(state.copyWith(status: LoginStatus.success));
  }
}
