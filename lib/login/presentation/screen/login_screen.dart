import 'package:bloc_demo/login/presentation/view_model/login_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../component/login_button.dart';
import '../component/password_textfield.dart';
import '../component/username_textfield.dart';
import '../state/login_state.dart';
import 'home_screen.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginViewModel>(
      create: (context) => LoginViewModel(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Login'),
          centerTitle: true,
        ),
        body: BlocListener<LoginViewModel, LoginState>(
          listener: (context, state) {
            if (state.status == LoginStatus.success) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const HomeScreen(),
                ),
              );
            }
          },
          child: const Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                UsernameTextField(),
                SizedBox(height: 10),
                PasswordTextField(),
                SizedBox(height: 10),
                LoginButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
