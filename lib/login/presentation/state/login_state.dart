import 'package:equatable/equatable.dart';

class LoginState extends Equatable {
  final String username;
  final String password;
  final LoginStatus status;
  final Exception? error;

  const LoginState({
    this.username = '',
    this.password = '',
    this.status = LoginStatus.initial,
    this.error,
  });

  LoginState copyWith({
    String? username,
    String? password,
    LoginStatus? status,
    Exception? error,
  }) {
    return LoginState(
      username: username ?? this.username,
      password: password ?? this.password,
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [username, password, status, error];
}

enum LoginStatus {
  initial,
  loading,
  success,
  error,
}