import 'package:bloc_demo/login/presentation/state/login_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../event/login_event.dart';
import '../view_model/login_viewmodel.dart';

class LoginButton extends StatelessWidget {
  const LoginButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginViewModel, LoginState>(
      builder: (context, state) {
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.deepPurple,
          ),
          onPressed: () {
            context.read<LoginViewModel>().add(LoginSubmitted());
          },
          child: (state.status == LoginStatus.loading)
              ? const SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator(
                    color: Colors.white,
                  ),
                )
              : const Text('Login', style: TextStyle(color: Colors.white)),
        );
      },
    );
  }
}
