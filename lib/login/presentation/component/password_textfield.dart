import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../event/login_event.dart';
import '../view_model/login_viewmodel.dart';

class PasswordTextField extends StatelessWidget {
  const PasswordTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'Password',
        hintText: 'Enter your password',
      ),
      onChanged: (value) {
        context.read<LoginViewModel>().add(
          LoginPasswordChanged(value),
        );
      },
    );
  }
}
