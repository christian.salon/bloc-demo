import 'package:bloc_demo/login/presentation/event/login_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../view_model/login_viewmodel.dart';

class UsernameTextField extends StatelessWidget {
  const UsernameTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Username',
        hintText: 'Enter your username',
      ),
      onChanged: (value) {
        context.read<LoginViewModel>().add(
          LoginUsernameChanged(value),
        );
      },
    );
  }
}