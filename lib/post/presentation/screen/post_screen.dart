import 'package:bloc_demo/post/data/di/post_service_locator.dart';
import 'package:bloc_demo/post/presentation/component/posts_list_view.dart';
import 'package:bloc_demo/post/presentation/event/post_event.dart';
import 'package:bloc_demo/post/presentation/viewmodel/post_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../component/add_button.dart';
import '../component/post_body_textfield.dart';
import '../component/post_title_textfield.dart';
import '../state/post_state.dart';

class PostScreen extends StatelessWidget {
  const PostScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PostViewModel>(
      create: (context) => PostViewModel(postRepository)
        ..add(
          PostsRequested(),
        ),
      child: BlocListener<PostViewModel, PostState>(
        listenWhen: (previous, current) => current.status != previous.status,
        listener: (context, state) {
          if (state.status == PostStatus.success) {
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Posts Loaded')),
            );
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Posts'),
          ),
          body: const Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(height: 20),
                PostTitleTextField(),
                SizedBox(height: 20),
                PostBodyTextField(),
                SizedBox(height: 20),
                AddButton(),
                SizedBox(height: 20),
                Expanded(
                  child: PostsListView(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
