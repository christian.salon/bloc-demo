import 'package:bloc_demo/post/presentation/event/post_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../viewmodel/post_viewmodel.dart';

class PostTitleTextField extends StatelessWidget {
  const PostTitleTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Title',
        hintText: 'Enter title',
      ),
      onChanged: (value) {
        context.read<PostViewModel>().add(
          PostTitleChanged(value),
        );
      },
    );
  }
}
