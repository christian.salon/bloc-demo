import 'package:bloc_demo/post/presentation/event/post_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../viewmodel/post_viewmodel.dart';

class PostBodyTextField extends StatelessWidget {
  const PostBodyTextField({super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Body',
      ),
      onChanged: (value) {
        context.read<PostViewModel>().add(
          PostBodyChanged(value),
        );
      },
    );
  }
}