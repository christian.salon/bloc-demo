import 'package:bloc_demo/post/presentation/state/post_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../viewmodel/post_viewmodel.dart';

class PostsListView extends StatelessWidget {
  const PostsListView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostViewModel, PostState>(
      buildWhen: (previous, current) => current.status != previous.status,
      builder: (context, state) {
        if (state.status == PostStatus.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return ListView.builder(
          itemCount: state.posts.length,
          itemBuilder: (context, index) {
            return Card(
              child: ListTile(
                title: Text(state.posts[index].title),
                subtitle: Text(state.posts[index].body),
              ),
            );
          },
        );
      }
    );
  }
}
