import 'package:bloc_demo/post/presentation/event/post_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/post_state.dart';
import '../viewmodel/post_viewmodel.dart';

class AddButton extends StatelessWidget {
  const AddButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostViewModel, PostState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return SizedBox(
          width: 200,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.deepPurple,
            ),
            onPressed: () {
              if (state.status == PostStatus.loading) {
                return;
              }

              context.read<PostViewModel>().add(PostSubmitted());
            },
            child: (state.status == PostStatus.loading)
                ? const SizedBox(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  )
                : const Text('Add Post', style: TextStyle(color: Colors.white)),
          ),
        );
      },
    );
  }
}
