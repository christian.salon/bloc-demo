import 'package:bloc_demo/post/presentation/event/post_event.dart';
import 'package:bloc_demo/post/presentation/state/post_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/model/post_input.dart';
import '../../data/repository/post_repository.dart';

class PostViewModel extends Bloc<PostEvent, PostState> {
  final PostRepository _postRepository;

  PostViewModel(this._postRepository) : super(const PostState()) {
    on<PostTitleChanged>(_onTitleChanged);
    on<PostBodyChanged>(_onBodyChanged);
    on<PostsRequested>(_onPostsRequested);
    on<PostSubmitted>(_onSubmitted);
  }

  void _onTitleChanged(
    PostTitleChanged event,
    Emitter<PostState> emit,
  ) {
    emit(state.copyWith(titleInput: event.title));
  }

  void _onBodyChanged(
    PostBodyChanged event,
    Emitter<PostState> emit,
  ) {
    emit(state.copyWith(bodyInput: event.body));
  }

  Future<void> _onPostsRequested(
    PostsRequested event,
    Emitter<PostState> emit,
  ) async {
    emit(state.copyWith(status: PostStatus.loading));

    final posts = await _postRepository.fetchPosts();

    emit(
      state.copyWith(
        status: PostStatus.success,
        posts: posts,
      ),
    );
  }

  Future<void> _onSubmitted(
    PostSubmitted event,
    Emitter<PostState> emit,
  ) async {
    if (state.titleInput.isEmpty || state.bodyInput.isEmpty) {
      emit(state.copyWith(status: PostStatus.error));
      return;
    }

    emit(state.copyWith(status: PostStatus.loading));

    final post = await _postRepository.addPost(
      PostInput(
        title: state.titleInput,
        body: state.bodyInput,
        userId: '1',
      ),
    );

    emit(
      state.copyWith(
        status: PostStatus.success,
        posts: [
          post,
          ...state.posts,
        ],
      ),
    );
  }
}
