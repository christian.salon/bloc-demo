abstract class PostEvent {}

class PostsRequested extends PostEvent {}

class PostTitleChanged extends PostEvent {
  final String title;

  PostTitleChanged(this.title);
}

class PostBodyChanged extends PostEvent {
  final String body;

  PostBodyChanged(this.body);
}

class PostSubmitted extends PostEvent {}

