import 'package:equatable/equatable.dart';

import '../../data/model/post.dart';

class PostState extends Equatable {
  final String titleInput;
  final String bodyInput;
  final List<Post> posts;
  final PostStatus status;
  final Exception? error;

  const PostState({
    this.titleInput = '',
    this.bodyInput = '',
    this.posts = const [],
    this.status = PostStatus.initial,
    this.error,
  });

  PostState copyWith({
    String? titleInput,
    String? bodyInput,
    List<Post>? posts,
    PostStatus? status,
    Exception? error,
  }) {
    return PostState(
      titleInput: titleInput ?? this.titleInput,
      bodyInput: bodyInput ?? this.bodyInput,
      posts: posts ?? this.posts,
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [
        titleInput,
        bodyInput,
        posts,
        status,
        error,
      ];
}

enum PostStatus {
  initial,
  loading,
  success,
  error,
}
