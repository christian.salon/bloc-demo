import 'package:bloc_demo/post/data/repository/post_repository_interface.dart';

import '../datasource/remote/post_remote_source.dart';
import '../model/post.dart';
import '../model/post_input.dart';

class PostRepository implements PostRepositoryInterface {
  final PostRemoteSource _remoteSource;

  const PostRepository(this._remoteSource);

  @override
  Future<List<Post>> fetchPosts() async {
    return await _remoteSource.getPosts();
  }

  @override
  Future<Post> addPost(PostInput input) async {
    return await _remoteSource.addPost(input);
  }
}