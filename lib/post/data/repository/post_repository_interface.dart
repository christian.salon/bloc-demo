import '../model/post.dart';
import '../model/post_input.dart';

abstract interface class PostRepositoryInterface {
  Future<List<Post>> fetchPosts();
  Future<Post> addPost(PostInput input);
}