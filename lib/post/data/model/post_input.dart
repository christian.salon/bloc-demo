import 'package:equatable/equatable.dart';

class PostInput extends Equatable {
  final String title;
  final String body;
  final String userId;

  const PostInput({
    required this.title,
    required this.body,
    required this.userId,
  });

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'body': body,
      'userId': userId,
    };
  }

  @override
  List<Object?> get props => [title, body, userId];
}