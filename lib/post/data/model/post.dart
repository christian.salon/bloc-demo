import 'package:equatable/equatable.dart';

class Post extends Equatable {
  final String title;
  final String body;
  final String userId;

  const Post({
    required this.title,
    required this.body,
    required this.userId,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      title: json['title'] as String? ?? '',
      body: json['body'] as String? ?? '',
      userId: json['userId'] as String? ?? '',
    );
  }

  @override
  List<Object?> get props => [title, body, userId];
}