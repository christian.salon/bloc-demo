import 'package:bloc_demo/post/data/model/post_input.dart';

import '../../model/post.dart';
// import 'package:http/http.dart' as http;

class PostRemoteSource {

  // static const String _baseUrl = 'https://jsonplaceholder.typicode.com';

  Future<List<Post>> getPosts() async {
    /// Mock API call

    return await Future.delayed(
      const Duration(seconds: 1),
      () => [
        const Post(
          title: 'Title 1',
          body: 'Body 1',
          userId: '1',
        ),
        const Post(
          title: 'Title 2',
          body: 'Body 2',
          userId: '2',
        ),
        const Post(
          title: 'Title 3',
          body: 'Body 3',
          userId: '3',
        ),
      ],
    );
  }

  Future<Post> addPost(PostInput input) async {
    return await Future.delayed(
      const Duration(seconds: 1),
      () => Post(
        title: input.title,
        body: input.body,
        userId: input.userId,
      ),
    );
  }
}