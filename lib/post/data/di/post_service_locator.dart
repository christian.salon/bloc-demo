import '../datasource/remote/post_remote_source.dart';
import '../repository/post_repository.dart';

final postRemoteSource = PostRemoteSource();
final postRepository = PostRepository(postRemoteSource);